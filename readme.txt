                      ━━━━━━━━━━━━━━━━━━━━━━━━━━━
                       FLANGER PLAYER / STREAMER
                      ━━━━━━━━━━━━━━━━━━━━━━━━━━━


                            [2021-12-20 pon]





1 About
═══════

  This project is an exercise in basic Digital Signal Processing - a
  program for `STM32 F7 46 G DISCOVERY' board[1] performing the
  "flanging" audio effect. It takes the live audio stream from the
  on-board mems microphones (U20, U21) or from the `wav' file provided
  on the USB pendrive (CN13, usb_fs), filters it, and outputs on the
  line-out (CN10) accessible with standard earphones. Four slide bars
  are being displayed on the touch screen to control the parameters of
  the filter. An option to record a custom `wav' file is provided, as
  long as USB key is connected.


2 Compilation
═════════════

  ┌────
  │ cd FlangerPlayerStreamer
  │ make
  └────


  The GNU Arm Embedded Toolchain is required (`arm-none-eabi-gcc'). The
  resulting binaries are placed in:

  ┌────
  │ FlangerPlayerStreamer/SW4STM32/STM32F7-DISCO/Debug/
  └────


3 How to use
════════════

3.1 Main menu
─────────────

  At the start of the program three buttons are visible, from left to
  right:
  1. "Play" (triangle)
  2. "Record" (circle)
  3. "Stream" (rectangle)

  The first two are active only upon inserting an USB key to `usb_fs'
  (an adapter may be needed from the most popular B-type plug to the
  micro-USB socket). Once it's discovered it's being searched for the
  `wav' files and the list is being displayed on the screen. Tapping it
  above the buttons will refresh the list.

  The streaming option is always available regardless of the pendrive
  being present or not.


3.2 Player
──────────

  Once entered a first file in the queue starts playing. Information
  about the file is being displayed on the center-left.

  Use the standard "pause" button to stop the play without leaving the
  player. Pressing it again will resume the audio. Pressing "stop" (full
  square) will leave the player and take the user back to the main menu.

  Use arrow buttons to switch the tracks to next / previous. Adjust the
  volume with "vol-", "vol+" buttons on the left and right.

  Right to the center the four slide bar buttons are provided to set the
  parameters of the flanging filter. They can be adjusted freely during
  the play changing the effect on the go. See [Flanging effect] for
  details about their meaning.


[Flanging effect] See section 4


3.3 Recorder
────────────

  Tap the circle to start recording. The sound is registered with the
  on-board mems microphones located on the right. The sound is being
  recorderd as-is, without any filtering. Once the "stop" button is
  pressed (square at the center) the sound is saved in the `Wave.wav'
  file. From the main menu open the player to play it back and apply the
  flanging filter. The maximum length of the recording is 30 seconds.


3.4 Streamer
────────────

  The streamer provides filtering applied to the live sound registered
  by the on-board microphones. The interface of the filter is the exact
  same as in the player. To exit the streamer reset the device.


4 Flanging filter
═════════════════

  The filter is being defined by the following formula:

  ┌────
  │ F(t) = (1 - w) f(t) + w f(t - D(t))
  └────


  where `f(t)' is the original audio wave and `F(t)' the filtered
  one. It's the result of the weighted mean (`w' parameter) between the
  original signal `f(t)' and the delayed one `f(t - D(t))'. The delay
  `D(t)' may itself be changing in time, according to various
  functions. In this application the sinus function is used

  ┌────
  │ D(t) = D0 + A sin(2 pi f t)
  └────


  where `D0' is the "base delay" modulated with time `t' at the
  frequency `f'. The following table links the four slide bars provided
  with the parameters just mentioned.

  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
          Parameter  Minimum  Maximum  Unit 
  ──────────────────────────────────────────
   Delay  `D0'             0       25  [ms] 
   Width  `A'              0       10  [ms] 
   Speed  `f'           0.01        2  [Hz] 
   Depth  `w'              0      100  [%]  
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

  Thus setting "Delay" and "Width" to 0 will produce the original sound,
  and setting "Depth" to 100% will get rid of the original sound
  completely leaving only the delayed one. The most pronounced effets
  are with "Width" around 50%, and all three parameters "Delay", "Width"
  and "Speed" being low, but nevertheless non-zero.



Footnotes
─────────

[1] <https://www.st.com/en/evaluation-tools/32f746gdiscovery.html>
