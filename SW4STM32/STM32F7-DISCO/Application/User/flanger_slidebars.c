#include <flanger_slidebars.h>

#define INIT_SLIDE_BAR(PARAM)                   \
    flangerBars[PARAM ## _SLIDE_BAR] =          \
        ValuedVertSlideBar_make(                \
            PARAM ## _NAME,                     \
            "[" PARAM ## _UNIT "]",             \
            PARAM ## _SLIDE_BAR_XMIN,           \
            PARAM ## _SLIDE_BAR_XMAX,           \
            PARAM ## _SLIDE_BAR_YMIN,           \
            PARAM ## _SLIDE_BAR_YMAX,           \
            FLANGER_SLIDEBARS_BUTTON_HEIGHT,    \
            PARAM ## _VALUE_MAX,                \
            PARAM ## _VALUE_MIN,                \
            PARAM ## _VALUE_INIT,               \
            0)

void FlangerSlidebars_init(
    ValuedVertSlideBar_t *flangerBars)
{
    INIT_SLIDE_BAR(DELAY);
    INIT_SLIDE_BAR(WIDTH);
    INIT_SLIDE_BAR(SPEED);
    INIT_SLIDE_BAR(DEPTH);    
}

void FlangerSlidebars_updateFilterState(
    ValuedVertSlideBar_t *flangerBars,
    FlangingFilterState_t *filter,
    size_t slideBarIndex)
{
    switch (slideBarIndex) {
    case DELAY_SLIDE_BAR: {
        filter->delay = ValuedVertSlideBar_getValue(
            &flangerBars[slideBarIndex]);
        break;
    }
    case WIDTH_SLIDE_BAR: {
        filter->width = ValuedVertSlideBar_getValue(
            &flangerBars[slideBarIndex]);
        break;
    }
    case SPEED_SLIDE_BAR: {
        filter->speed = ValuedVertSlideBar_getValue(
            &flangerBars[slideBarIndex]);
        break;
    }
    case DEPTH_SLIDE_BAR: {
        filter->depth = ValuedVertSlideBar_getValue(
            &flangerBars[slideBarIndex]);
        break;
    }
    }
}

void FlangerSlidebars_updateFilterStateAll(
    ValuedVertSlideBar_t *flangerBars,
    FlangingFilterState_t *filter)
{
    for (size_t i = 0; i < FLANGER_SLIDEBARS_NUM; ++i) {
        FlangerSlidebars_updateFilterState(
            flangerBars, filter, i);        
    }
}

void FlangerSlidebars_moveActiveSlidebars(
    ValuedVertSlideBar_t *flangerBars,
    FlangingFilterState_t *filter,
    int16_t y)
{
    for (size_t i = 0; i < FLANGER_SLIDEBARS_NUM; ++i) {
        if (flangerBars[i].slideBar.frame.active) {
            ValuedVertSlideBar_setButtonPos_touch(
                &flangerBars[i],
                y);
            ValuedVertSlideBar_display(&flangerBars[i]);
            FlangerSlidebars_updateFilterState(
                flangerBars,
                filter,
                i);
        }
    }
}

void FlangerSlidebars_activateTouchedSlidebar(
    ValuedVertSlideBar_t *flangerBars,
    Point_t touchPoint)
{
    for (size_t i = 0; i < FLANGER_SLIDEBARS_NUM; ++i) {
        if (Rect_isPointInside(
                &flangerBars[i].slideBar.frame.rect,
                touchPoint)) {
            TouchableRect_setActive(
                &flangerBars[i].slideBar.frame, 1);
        }
    }
}

void FlangerSlidebars_deactivateAllSlidebars(
    ValuedVertSlideBar_t *flangerBars)
{
    for (size_t i = 0; i < FLANGER_SLIDEBARS_NUM; ++i) {
        TouchableRect_setActive(
            &flangerBars[i].slideBar.frame, 0);
    }
}

void FlangerSlidebars_display(
    ValuedVertSlideBar_t *flangerBars)
{    
    for (int i = 0; i < FLANGER_SLIDEBARS_NUM; ++i) {
        ValuedVertSlideBar_display(&flangerBars[i]);
    }
}
