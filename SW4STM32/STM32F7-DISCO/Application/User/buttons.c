#include <buttons.h>
#include <stm32746g_discovery_lcd.h>
#include <lcd_log_conf.h>

#define TOUCHABLE_RECT_INACTIVE_COLOR LCD_COLOR_LIGHTGREEN
#define TOUCHABLE_RECT_ACTIVE_COLOR   LCD_COLOR_RED

/* Rect_t ********************************************************************/

static void Rect_init(
    Rect_t *this,
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax)
{
    this->topLeft.x = xMin;
    this->topLeft.y = yMin;
    this->width = xMax - xMin;
    this->height = yMax - yMin;
}

Rect_t Rect_make(
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax)
{
    Rect_t result;
    Rect_init(&result, xMin, xMax, yMin, yMax);
    return result;
}

int16_t Rect_getRight(Rect_t *rect) {
    return rect->topLeft.x + rect->width;
}

int16_t Rect_getBottom(Rect_t *rect) {
    return rect->topLeft.y + rect->height;
}

_Bool Rect_isPointInside(
    Rect_t *rect,
    Point_t point)
{
    return (point.x > rect->topLeft.x)
        && (point.x < Rect_getRight(rect))
        && (point.y > rect->topLeft.y)
        && (point.y < Rect_getBottom(rect));
}

_Bool Rect_isPointInside_TS(Rect_t *rect,
                            TS_StateTypeDef *ts,
                            int touch_indx) {
    Point_t touch = { ts->touchX[touch_indx],
                      ts->touchY[touch_indx] };
    return Rect_isPointInside(rect, touch);
}

Point_t Rect_getCenter(Rect_t *rect) {
    Point_t res = {
        rect->topLeft.x + rect->width / 2,
        rect->topLeft.y + rect->height / 2
    };
    return res;
}

void Rect_placeByCenter(
    Rect_t *rect,
    Point_t center)
{
    rect->topLeft.x = center.x - rect->width / 2;
    rect->topLeft.y = center.y - rect->height / 2;
}

void Rect_display_currColor(
    Rect_t *rect,
    _Bool fill)
{
    if (fill) {
        BSP_LCD_FillRect(rect->topLeft.x,
                         rect->topLeft.y,
                         rect->width,
                         rect->height);
    } else {
        BSP_LCD_DrawRect(rect->topLeft.x,
                         rect->topLeft.y,
                         rect->width,
                         rect->height);
    }
}

void Rect_display(
    Rect_t *rect,
    uint32_t color,
    _Bool fill)
{
    uint32_t currColor = BSP_LCD_GetTextColor();
    BSP_LCD_SetTextColor(color);
    Rect_display_currColor(rect, fill);
    BSP_LCD_SetTextColor(currColor);
}

/* TouchableRect_t ***********************************************************/


static void TouchableRect_init(
    TouchableRect_t *this,
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    _Bool active,
    _Bool filled)
{
    Rect_init(&this->rect, xMin, xMax, yMin, yMax);
    this->active = active;
    this->filled = filled;
}

TouchableRect_t TouchableRect_make(
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    _Bool active,
    _Bool filled)
{
    TouchableRect_t result;
    TouchableRect_init(&result,
                       xMin, xMax, yMin, yMax,
                       active, filled);
    return result;
}

void TouchableRect_setActive(
    TouchableRect_t *this,
    _Bool active)
{
    if (this->active != active)
    {
        this->active = active;
        TouchableRect_display(this);
    }
}

void TouchableRect_display(TouchableRect_t *this)
{
    uint32_t currColor = BSP_LCD_GetTextColor();
    // BSP_LCD_SetTextColor(BSP_LCD_GetBackColor());
    // Rect_display_currColor(&this->rect, fill);
    if (this->active) {
        BSP_LCD_SetTextColor(TOUCHABLE_RECT_ACTIVE_COLOR);
    } else {
        BSP_LCD_SetTextColor(TOUCHABLE_RECT_INACTIVE_COLOR);
    }
    Rect_display_currColor(&this->rect, this->filled);
    BSP_LCD_SetTextColor(currColor);
}


/* VertSlideBar_t ************************************************************/

static void VertSlideBar_init(
    VertSlideBar_t *this,
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    int16_t buttonHeight,
    int16_t buttonPos,
    _Bool active)
{
    TouchableRect_init(&this->frame,
                       xMin, xMax, yMin, yMax,
                       active, 0);
    this->buttonHeight = buttonHeight;
    this->buttonPos = buttonPos;
}

VertSlideBar_t VertSlideBar_make(
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    int16_t buttonHeight,
    int16_t buttonPos,
    _Bool active)
{
    VertSlideBar_t result;
    VertSlideBar_init(&result,
                      xMin, xMax, yMin, yMax,
                      buttonHeight, buttonPos, active);
    return result;
}

int16_t VertSlideBar_getMaxButtonPos(VertSlideBar_t *this) {
    return this->frame.rect.height - this->buttonHeight;;
}

TouchableRect_t VertSlideBar_getButtonTouchableRect(
    VertSlideBar_t *this,
    _Bool active)
{
    return TouchableRect_make(
        this->frame.rect.topLeft.x,
        this->frame.rect.topLeft.x + this->frame.rect.width,
        this->frame.rect.topLeft.y + this->buttonPos,
        this->frame.rect.topLeft.y + this->buttonPos + this->buttonHeight,
        active,
        1);
}

void VertSlideBar_setButtonPos(
    VertSlideBar_t *this,
    int16_t newButtonPos)
{
    this->buttonPos =
        MAX(0,
            MIN(newButtonPos,
                VertSlideBar_getMaxButtonPos(this)));
}

/* Given user's touch point set the sliding button */
void VertSlideBar_setButtonPos_touch(
    VertSlideBar_t *this,
    int16_t y)
{
    VertSlideBar_setButtonPos(this,
                              y
                              - this->buttonHeight / 2
                              - this->frame.rect.topLeft.y);
}

float VertSlideBar_getButtonPos_rel(VertSlideBar_t *this) {
    return (float)this->buttonPos
        / VertSlideBar_getMaxButtonPos(this);
}

void VertSlideBar_setButtonPos_rel(
    VertSlideBar_t *this,
    float relButtonPos)
{
    VertSlideBar_setButtonPos(
        this,
        (int16_t)(VertSlideBar_getMaxButtonPos(this)
                   * relButtonPos));
}

void VertSlideBar_setActive(
    VertSlideBar_t *this,
    _Bool active)
{
    if (this->frame.active != active) {
        this->frame.active = active;
        VertSlideBar_display(this);
    }
}

void VertSlideBar_display(VertSlideBar_t *this) {
    Rect_display(&this->frame.rect,
                 BSP_LCD_GetBackColor(),
                 1);
    TouchableRect_display(&this->frame);
    TouchableRect_t button =
        VertSlideBar_getButtonTouchableRect(
            this, 0);
    TouchableRect_display(&button);
}

/* ValuedVertSlideBar_t ******************************************************/

ValuedVertSlideBar_t ValuedVertSlideBar_make(
    char* name,
    char* unit,
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    int16_t buttonHeight,
    float valueTop,
    float valueBottom,
    float initVal,
    _Bool active)
{
    ValuedVertSlideBar_t result;
    VertSlideBar_init(&result.slideBar,
                      xMin,
                      xMax,
                      yMin,
                      yMax,
                      buttonHeight,
                      0,
                      0);
    result.valueTop = valueTop;
    result.valueBottom = valueBottom;
    result.name = name;
    result.unit = unit;
    ValuedVertSlideBar_setValue(&result, initVal);
    return result;
}

float ValuedVertSlideBar_getValue(ValuedVertSlideBar_t *this) {
    return this->valueTop +
        (this->valueBottom - this->valueTop)
        * VertSlideBar_getButtonPos_rel(&this->slideBar);
}

void ValuedVertSlideBar_setValue(
    ValuedVertSlideBar_t *this,
    float value)
{
    VertSlideBar_setButtonPos_rel(
        &this->slideBar,
        (value - this->valueTop)
        / (this->valueBottom - this->valueTop));
}

/* Given user's touch point set the sliding button */
void ValuedVertSlideBar_setButtonPos_touch(
    ValuedVertSlideBar_t *this,
    int16_t y)
{
    VertSlideBar_setButtonPos_touch(&this->slideBar, y);
}

#define ABS(x) ((x) >= 0 ? (x) : -(x))

void ValuedVertSlideBar_display(ValuedVertSlideBar_t *this) {
    char str[32];
    uint32_t currColor = BSP_LCD_GetTextColor();

    VertSlideBar_display(&this->slideBar);

    BSP_LCD_SetFont(&LCD_LOG_TEXT_FONT);
    BSP_LCD_SetTextColor(SLIDE_BAR_TEXT_COLOR);

    sprintf(str, "%s:", this->name);
    BSP_LCD_DisplayStringAt(
        this->slideBar.frame.rect.topLeft.x,
        this->slideBar.frame.rect.topLeft.y
        - LINE(1)
        - SLIDE_BAR_TEXT_SPACE,
        (uint8_t*)str,
        LEFT_MODE);

    float val = ValuedVertSlideBar_getValue(this);
    int valFloor = (int)val;
    int valHundreds = (int) (val * 100) - valFloor * 100;
    unsigned valHundredsU = (unsigned)(ABS(valHundreds));

    int16_t bottom =
        Rect_getBottom(&this->slideBar.frame.rect);

    sprintf(str, "%3d.%02u",
            valFloor,
            valHundredsU);

    BSP_LCD_DisplayStringAt(
        this->slideBar.frame.rect.topLeft.x,
        bottom + SLIDE_BAR_TEXT_SPACE,
        (uint8_t*)str,
        LEFT_MODE);

    sprintf(str, "%6s",
            this->unit);

    BSP_LCD_DisplayStringAt(
        this->slideBar.frame.rect.topLeft.x,
        bottom + SLIDE_BAR_TEXT_SPACE + LINE(1),
        (uint8_t*)str,
        LEFT_MODE);


    BSP_LCD_SetTextColor(currColor);
}

