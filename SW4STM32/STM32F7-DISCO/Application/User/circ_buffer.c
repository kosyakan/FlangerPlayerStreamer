#include <circ_buffer.h>

static void CircChunkBuffer_init(
    CircChunkBuffer_t *this,
    sample_t *buf,
    size_t size,
    size_t chunkSize)
{
    this->buf       = buf;
    this->chunkSize = chunkSize;
    this->size      = size;
    this->offset    = 0;
}

CircChunkBuffer_t CircChunkBuffer_make(
    sample_t *buf,
    size_t size,
    size_t chunkSize)
{
    CircChunkBuffer_t result;
    CircChunkBuffer_init(&result, buf, size, chunkSize);
    return result;
}

static void CircChunkBuffer_forward(
    CircChunkBuffer_t *this,
    size_t elements)
{
    this->offset =
        (this->offset + elements) % this->size;
}

FRESULT CircChunkBuffer_forwardChunk(
    CircChunkBuffer_t *this)
{
    CircChunkBuffer_forward(this, this->chunkSize);
    return FR_OK;
}

inline sample_t CircChunkBuffer_get(
    CircChunkBuffer_t *this,
    int i)
{
    return this->buf[MODULO((int)(this->offset) + i,
                            this->size)];
}

inline void CircChunkBuffer_set(
    CircChunkBuffer_t *this,
    int i,
    sample_t elem)
{
    this->buf[MODULO((int)(this->offset) + i, this->size)]
        = elem;
}


FileReadingBuffer_t FileReadingBuffer_make(
    sample_t *buf,
    size_t inChunkSize,
    size_t inChunksNum,
    size_t outChunkSize,
    FIL *wavFile,
    uint32_t fileSize,
    uint32_t byteRate,
    uint32_t futureSamplesMargin)
{
    FileReadingBuffer_t result;
    result.wavFile             = wavFile;
    result.fileSize            = fileSize;
    result.byteRate            = byteRate;
    result.futureSamplesMargin = futureSamplesMargin;
    size_t size = inChunkSize * inChunksNum;
    result.bytesRead = 0;
    CircChunkBuffer_init(&result.filePersp,
                         buf,
                         size,
                         inChunkSize);
    CircChunkBuffer_init(&result.audioPersp,
                         buf,
                         size,
                         outChunkSize);
    return result;
}

/* Read `inChunkSize' elements from the file `wavFile' and put
 * the data in `buf', starting at `inPerspective.offset', padding
 * with zeros if there was not enough. In case the file is
 * finished the function will be simply filling
 * `inPerspective.buf' with zeros. */
static FRESULT FileReadingBuffer_readChunkFromFile(
    FileReadingBuffer_t *this)
{
    uint32_t bytesRead;
    FRESULT result =
        f_read(this->wavFile,
               (void*)(this->filePersp.buf
                       + this->filePersp.offset),
               this->filePersp.chunkSize
               * sizeof(sample_t),
               (void*) &bytesRead);
    this->bytesRead += bytesRead;
    uint32_t elementsRead = bytesRead / sizeof(sample_t);
    for (size_t i = elementsRead;
         i < this->filePersp.chunkSize;
         ++i)
    {
        this->filePersp.buf[this->filePersp.offset + i]
            = 0;
    }
    CircChunkBuffer_forwardChunk(&this->filePersp);
    return result;
}

/* Return the maximum `n' for which
 * `CircChunkBuffer_get(this->outPerspective, n)' can be called
 * without getting undefined data. */
static size_t FileReadingBuffer_pointersDistance(
    FileReadingBuffer_t *this)
{
    if (this->filePersp.offset < this->audioPersp.offset) {
        return this->filePersp.offset
            + this->filePersp.size
            - this->audioPersp.offset;
    } else {
        return this->filePersp.offset
            - this->audioPersp.offset;
    }
}

FRESULT FileReadingBuffer_forwardChunk(
    CircChunkBuffer_t *gThis)
{
    FileReadingBuffer_t *this = (FileReadingBuffer_t*)gThis;
    FRESULT result;
    if (FileReadingBuffer_pointersDistance(this)
        < this->audioPersp.chunkSize + this->futureSamplesMargin) {
        result = FileReadingBuffer_readChunkFromFile(this);
    } else {
        result = FR_OK;
    }
    CircChunkBuffer_forwardChunk(&this->audioPersp);
    return result;
}

_Bool FileReadingBuffer_isFileFinished(FileReadingBuffer_t *this) {
    return this->bytesRead >= this->fileSize;
}

uint32_t FileReadingBuffer_elapsedTime(FileReadingBuffer_t *this) {
    return this->bytesRead / this->byteRate;
}
