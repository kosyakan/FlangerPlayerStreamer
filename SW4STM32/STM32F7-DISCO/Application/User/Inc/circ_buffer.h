#ifndef CIRC_BUFFER_H_
#define CIRC_BUFFER_H_

#include <stdint.h>
#include <ff.h>


/* Implement the actual `modulo' reduction, the one which
 * returns `4' for `-1 mod 5', not `-1'.  */
#define MODULO(x,n) ((x) >= 0) ?                \
  (int)(x) % (int)(n) :                         \
  (int)(n) - ((-(int)(x) - 1) % (int)(n)) - 1

typedef uint16_t sample_t;       // type of the single audio sample

/* CircChunkBuffer_t *********************************************************/

/*
 * Represent a circular buffer which is being read from, or written to, in
 * chunks.
 * 
 * Elements are accesible with function `CircChunkBuffer_get', where
 * `CircChunkBuffer_get(buf, 0)' represents the "current" element, whatever
 * that means in a particular case.
 * 
 * Elements can be indexed with positive and negative numbers, such that
 * 
 * ...  
 * `CircularChunkBuffer_get(buf, -2)' : previous from previous element
 * `CircularChunkBuffer_get(buf, -1)' : previous element
 * `CircularChunkBuffer_get(buf, 0)' : current element
 * `CircularChunkBuffer_get(buf, 1)' : next element
 * `CircularChunkBuffer_get(buf, 2)' : next to the next element 
 * ...
 * 
 * as well as indexed with numbers exceeding the size of the array, such
 * that `CircChunkBuffer_get(buf, buf.size + 1)' returns the same element
 * as `CircChunkBuffer_get(buf, 2)'.
 * 
 * The "current" element from which indexing starts can be shifted with
 * `CircChunkBuffer_forward', such that
 * 
 * ,----
 * | CircChunkBuffer_forward(x);
 * | val = CircChunkBuffer_get(buf, 0);
 * `----
 * 
 * will result in `val' being the same as in
 * 
 * ,----
 * | val = CircChunkBuffer_get(buf, x);
 * `----
 * 
 * Invariant: `offset' assumes only the multiples of `chunkSize' modulo
 * `size'. For the special, and very useful, case of `size' being the
 * multiple of `chunkSize' itself `offset' assumes no other
 * values than
 * 
 * ,----
 * | 0
 * | chunkSize
 * | chunkSize * 2
 * | ...
 * | chunkSize * (N-1)
 * `----
 * 
 * where `N = size / chunkSize', (the value changes are done by
 * `CircChunkBuffer_forwardChunk'). That means a contiguous
 * memory reading or writing of `chunkSize' length starting at
 * `offset' can be done at any moment without falling out of the
 * buffer or interfering with other chunks. This property is used
 * when reading ".wav" file, as well as when filling and emptying
 * DMA buffers during streaming.
 */

typedef struct {
    sample_t *buf;
    size_t size;
    size_t chunkSize;
    size_t offset;
} CircChunkBuffer_t;

CircChunkBuffer_t CircChunkBuffer_make(
    sample_t *buf,
    size_t size,
    size_t chunkSize);

FRESULT CircChunkBuffer_forwardChunk(
    CircChunkBuffer_t *this);

sample_t CircChunkBuffer_get(
    CircChunkBuffer_t *this,
    int i);

void CircChunkBuffer_set(
    CircChunkBuffer_t *this,
    int i,
    sample_t elem);


typedef struct {
    /* Circular buffers sharing the same memory, but operating
     * with different chunk sizes */

    /* By putting `audioPersp' as the first field in the struct
     * it's possible to safely treat `FileReadingBuffer_t*'
     * pointer as `CircChunkBuffer_t*' pointer, thus opening all
     * the functions defined for `CircChunkBuffer_t', in
     * particular `CircChunkBuffer_get' (used in
     * `FlangingFilterState_filterAudio'). A bit of C++
     * emulation. */
    CircChunkBuffer_t audioPersp;
    CircChunkBuffer_t filePersp;
    uint32_t bytesRead;

    /* Fields constant during a single file streaming */

    FIL *wavFile;
    uint32_t fileSize;
    uint32_t byteRate;
    /*
     * `futureSamplesMargin': when deciding whether to read from
     * `wavFile' another chunk or not make sure that at least
     * this many future samples will be available. In other
     * words, so that the call
     *
     * ,----
     * | CircChunkBuffer_get(&audioPersp, futureSamplesMargin)
     * `----
     *
     * would be guaranteed to return a defined value.
     */
    uint32_t futureSamplesMargin;

} FileReadingBuffer_t;

FileReadingBuffer_t FileReadingBuffer_make(
    sample_t *buf,
    size_t fileChunkSize,
    size_t fileChunksNum,
    size_t audioChunkSize,
    FIL *wavFile,
    uint32_t fileSize,
    uint32_t byteRate,
    uint32_t futureSamplesMargin);

/* Forward by `outPerspective.chunkSize' */
FRESULT FileReadingBuffer_forwardChunk(
    CircChunkBuffer_t *this);


_Bool FileReadingBuffer_isFileFinished(FileReadingBuffer_t *this);

/* Return the amount of data read, in seconds */
uint32_t FileReadingBuffer_elapsedTime(FileReadingBuffer_t *this);


#endif /* CIRC_BUFFER_H_ */
