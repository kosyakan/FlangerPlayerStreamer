#ifndef FLANGER_H
#define FLANGER_H

#include <stdint.h>
#include <ff.h>

#include "circ_buffer.h"

/*
 * Flanging filter: given the signal `f(t)', produce the signal `F(t)'
 * according to the formula:
 *
 * ,----
 * | F(t) = (1 - w) f(t) + w f(t - D(t))
 * `----
 *
 *
 * for some signal delaying function `D(t)', in this case
 *
 * ,----
 * | D(t) = D0 + A sin(2 pi f t)
 * `----
 *
 *
 * where:
 *
 * `t'
 *       time,
 * `D0'
 *       basic delay modulated with tim by `D(t)' function,
 * `A'
 *       the "width" of the flanging effect - amplitude of the `D(t)'
 *       modulation,
 * `f'
 *       the "speed" of the flanging effect - how fast `D0' is being
 *       modulated,
 * `w in [0,1]'
 *       the "depth" of the flanging effect - the ratio of mixing the
 *       original signal with its delayed copy
 */

typedef struct {
    // User-customizable:
    float delay;                // `D0' [ms]
    float width;                // `A'  [ms]
    float speed;                // `f'  [Hz]
    float depth;                // `w'  [%]
    // Internal:
    uint32_t time;              // `t' at the end of the last
                                // audio buffer update, [samples]
} FlangingFilterState_t;

FlangingFilterState_t FlangingFilterState_make(
    float delay,
    float width,
    float speed,
    float depth);

// samplingFreq: [samples / s] = [Hz]
int FlangingFilterState_delaySamples(
    FlangingFilterState_t *this,
    uint32_t samplingFreq);

// samplingFreq: [samples / s] = [Hz]
int FlangingFilterState_widthSamples(
    FlangingFilterState_t *this,
    uint32_t samplingFreq);

/*
 * Take the field `speed', given in `[Hz] = [samples / s]' units
 * (circular frequency, not radial), used in an expression
 *
 * ,----
 * | sin(2 * pi * speed * t)
 * `----
 *
 * for `t' being time the given in seconds, and convert it into a
 * parameter `speedUnitless', used in an equivalent expression
 *
 * ,----
 * | sin(2 * pi * speed * t) = sin(speedUnitless * tSamples)
 * `----
 *
 * for `tSamples' being the time given in samples at a given
 * sampling rate `samplingFreq'.
 */
float FlangingFilterState_speedUnitless(
    FlangingFilterState_t *this,
    uint32_t samplingFreq);

/* Simple percent to [0,1] conversion */
float FlangingFilterState_depthUnitless(
    FlangingFilterState_t *this);

/* The flanging filter is time-dependent, and the time reference
 * frame used in the player is being shifted in chunks to fix the
 * current moment at 0. This function updates the filter's state
 * to reflect that. */
void FlangingFilterState_forward(
    FlangingFilterState_t *this,
    uint32_t samples);

/* Move data from `inBuf' to `outBuf' passing it through the
 * flanging filter defined by `filter'. The amount of
 * samples moved is given by `outBuf->chunkSize'.
 *
 * The buffer `outBuf' is being "forwarded" by one chunk of data
 * (see `CircChunkBuffer_forwardChunk' function). The `inBuf' as
 * well, but using the given `inBufForwardChunk' function,
 * providing some flexibility in the precise interpretation of
 * "forwarding `inBuf'", which may differ between different audio
 * sources. 
 *
 * At the moment two sources are being used: wav file and the
 * stream from the microphone. The latter just moves the pointer
 * around the DMA memory area being filled by the audio module
 * (`CircChunkBuffer_forwardChunk' used), but the former performs
 * readings from the file when needed
 * (`FileReadingBuffer_forwardChunk' used). The `FRESULT'-type
 * return value is provided for this exact case. When reading
 * from memory the filter always returns `FR_OK' value.
 *
 * The state of the `filter' is being changed as well
 * `FlangingFilterState_forward' function.
 */
FRESULT FlangingFilterState_filterAudio(
    FlangingFilterState_t *filter,
    CircChunkBuffer_t *inBuf,
    FRESULT (*inBufForwardChunk)(CircChunkBuffer_t *this),
    CircChunkBuffer_t *outBuf,
    uint32_t samplingFreq,
    uint16_t channelsNum);

#endif /* FLANGER_H */
