/* Generated file */

#ifndef SLIDEBAR_CONSTS_H
#define SLIDEBAR_CONSTS_H


#define DELAY_SLIDE_BAR 0
#define WIDTH_SLIDE_BAR 1
#define SPEED_SLIDE_BAR 2
#define DEPTH_SLIDE_BAR 3

#define DELAY_NAME "Delay"
#define WIDTH_NAME "Width"
#define SPEED_NAME "Speed"
#define DEPTH_NAME "Depth"

#define DELAY_UNIT "ms"
#define WIDTH_UNIT "ms"
#define SPEED_UNIT "Hz"
#define DEPTH_UNIT "%"

#define DELAY_VALUE_MIN 0.0f // [ms]
#define WIDTH_VALUE_MIN 0.0f // [ms]
#define SPEED_VALUE_MIN 0.02f // [Hz]
#define DEPTH_VALUE_MIN 0.0f // [%]

#define DELAY_VALUE_MAX 25.0f // [ms]
#define WIDTH_VALUE_MAX 10.0f // [ms]
#define SPEED_VALUE_MAX 2.0f // [Hz]
#define DEPTH_VALUE_MAX 100.0f // [%]

#define DELAY_VALUE_INIT 0.0f // [ms]
#define WIDTH_VALUE_INIT 0.0f // [ms]
#define SPEED_VALUE_INIT 1.0f // [Hz]
#define DEPTH_VALUE_INIT 0.0f // [%]

#define DELAY_SLIDE_BAR_XMIN 220 // [px]
#define WIDTH_SLIDE_BAR_XMIN 286 // [px]
#define SPEED_SLIDE_BAR_XMIN 352 // [px]
#define DEPTH_SLIDE_BAR_XMIN 419 // [px]

#define DELAY_SLIDE_BAR_XMAX 265 // [px]
#define WIDTH_SLIDE_BAR_XMAX 331 // [px]
#define SPEED_SLIDE_BAR_XMAX 397 // [px]
#define DEPTH_SLIDE_BAR_XMAX 464 // [px]

#define DELAY_SLIDE_BAR_YMIN 60 // [px]
#define WIDTH_SLIDE_BAR_YMIN 60 // [px]
#define SPEED_SLIDE_BAR_YMIN 60 // [px]
#define DEPTH_SLIDE_BAR_YMIN 60 // [px]

#define DELAY_SLIDE_BAR_YMAX 164 // [px]
#define WIDTH_SLIDE_BAR_YMAX 164 // [px]
#define SPEED_SLIDE_BAR_YMAX 164 // [px]
#define DEPTH_SLIDE_BAR_YMAX 164 // [px]

#endif /* SLIDEBAR_CONSTS_H */