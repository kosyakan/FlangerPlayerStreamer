#ifndef APPLICATION_USER_INC_FLANGER_SLIDEBARS_H_
#define APPLICATION_USER_INC_FLANGER_SLIDEBARS_H_

#include <stm32746g_discovery_ts.h>

#include <flanger.h>
#include <buttons.h>

/* Generated file, contains definitions of slide bar geometries */
#include "slidebar_consts.h"

/*
 *   Delay:   Width:   Speed:   Depth:
 *   +-----+  +-----+  +-----+  +-----+
 *   |     |  |     |  |     |  |     |
 *   |     |  |     |  +-----+  |     |
 *   +-----+  |     |  |     |  |     |
 *   |     |  |     |  |     |  +-----+
 *   +-----+  +-----+  |     |  |     |
 *   |     |  |     |  |     |  +-----+
 *   |     |  +-----+  |     |  |     |
 *   |     |  |     |  |     |  |     |
 *   |     |  |     |  |     |  |     |
 *   +-----+  +-----+  +-----+  +-----+
 *    24.26     1.66     2.00    46.42
 *     [ms]     [ms]     [Hz]      [%]
 */

#define FLANGER_SLIDEBARS_NUM 4u
#define FLANGER_SLIDEBARS_BUTTON_HEIGHT 20 /* [px] */

/* Assume all `flangerBars' ararys are of `FLANGER_SLIDEBARS_NUM'
 * length. */

void FlangerSlidebars_init(
    ValuedVertSlideBar_t *flangerBars);

void FlangerSlidebars_updateFilterState(
    ValuedVertSlideBar_t *flangerBars,
    FlangingFilterState_t *filter,
    size_t indx);

void FlangerSlidebars_updateFilterStateAll(
    ValuedVertSlideBar_t *flangerBars,
    FlangingFilterState_t *filter);

void FlangerSlidebars_moveActiveSlidebars(
    ValuedVertSlideBar_t *flangerBars,
    FlangingFilterState_t *filter,
    int16_t y);

void FlangerSlidebars_activateTouchedSlidebar(
    ValuedVertSlideBar_t *flangerBars,
    Point_t touchPoint);

void FlangerSlidebars_deactivateAllSlidebars(
    ValuedVertSlideBar_t *flangerBars);

void FlangerSlidebars_display(
    ValuedVertSlideBar_t *flangerBars);

#endif /* APPLICATION_USER_INC_FLANGER_SLIDEBARS_H_ */
