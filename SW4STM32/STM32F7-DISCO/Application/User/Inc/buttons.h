#ifndef APPLICATION_USER_SLIDE_BUTTON_H_
#define APPLICATION_USER_SLIDE_BUTTON_H_

#include <stdint.h>
#include <stm32746g_discovery_ts.h>

/* Space between the slide bar and its description */
#define SLIDE_BAR_TEXT_SPACE 5             /* [px] */
#define SLIDE_BAR_TEXT_COLOR LCD_COLOR_CYAN

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) > (b) ? (b) : (a))

/*
 *     |              x
 *   --+--------------->
 *     |
 *     |
 *     |
 *   y |
 *     v
 */

/* Point_t *******************************************************************/

typedef struct {
    int16_t x;
    int16_t y;
} Point_t;

/* Rect_t ********************************************************************/

typedef struct {
    Point_t topLeft;
    int16_t width;
    int16_t height;
} Rect_t;

Rect_t Rect_make(
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax);

int16_t Rect_getBottom(Rect_t *this);
int16_t Rect_getRight(Rect_t *this);

_Bool Rect_isPointInside(
    Rect_t *rect,
    Point_t point);

_Bool Rect_isPointInside_TS(
    Rect_t *rect,
    TS_StateTypeDef *ts,
    int touch_indx);

Point_t Rect_getCenter(Rect_t *this);
void Rect_placeByCenter(
    Rect_t *this,
    Point_t center);

void Rect_display_currColor(
    Rect_t *this,
    _Bool fill);
void Rect_display(
    Rect_t *this,
    uint32_t color,
    _Bool fill);

/* TouchableRect_t ***********************************************************/

/* A rectangle which can be activated or disactivated - good for
 * representing buttons. */
typedef struct {
    Rect_t rect;
    _Bool active;
    _Bool filled;
} TouchableRect_t;

TouchableRect_t TouchableRect_make(
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    _Bool active,
    _Bool filled);

void TouchableRect_setActive(
    TouchableRect_t *this,
    _Bool active);

void TouchableRect_display(TouchableRect_t *this);

/* VertSlideBar_t ************************************************************/

/*
 *
 *      tlx
 *  tly  +---------+  +          +
 *       |         |  |          |
 *       |         |  | bPos     |
 *       |         |  |          |
 *       +---------+  +          |
 *       |         |  |          | frame.height
 *       |         |  | bHeight  |
 *       |         |  |          |
 *       +---------+  +          |
 *       |         |             |
 *       +---------+             +
 *
 *       +---------+
 *       frame.width
 *
 *    tly     : frame.topLeft.y
 *    tlx     : frame.topLeft.x
 *    bPos    : buttonPos
 *    bHeight : buttonHeight
 *
 */

typedef struct {
    TouchableRect_t frame;
    int16_t buttonHeight;
    int16_t buttonPos;
} VertSlideBar_t;

VertSlideBar_t VertSlideBar_make(
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    int16_t buttonHeight,
    int16_t buttonPos,
    _Bool active);

int16_t VertSlideBar_getMaxButtonPos(VertSlideBar_t *this);

TouchableRect_t VertSlideBar_getButtonTouchableRect(
    VertSlideBar_t *this,
    _Bool active);

void VertSlideBar_setButtonPos(
    VertSlideBar_t *this,
    int16_t newButtonPos);

/* Given user's touch point set the sliding button */
void VertSlideBar_setButtonPos_touch(
    VertSlideBar_t *this,
    int16_t y);

/* Value from 0 to 1, where 0 is the top-most, and 1 is the
 * bottom-most position of the button */
float VertSlideBar_getButtonPos_rel(
    VertSlideBar_t *this);

void VertSlideBar_setButtonPos_rel(
    VertSlideBar_t *this,
    float relButtonPos);

void VertSlideBar_setActive(
    VertSlideBar_t *this,
    _Bool active);

/* Given user's touch point set the sliding button */
void VertSlideBar_display(VertSlideBar_t *this);

/* ValuedVertSlideBar_t ******************************************************/

/* A vertical slide bar with associated values at the
 * ends. Provides interface to aqcuire the value being
 * set. */

typedef struct {
    VertSlideBar_t slideBar;
    float valueTop;
    float valueBottom;
    char* name;                /* Name of the numerical value being set */
    char* unit;                /* Name of the unit of the value being set */
} ValuedVertSlideBar_t;

ValuedVertSlideBar_t ValuedVertSlideBar_make(
    char* name,
    char* unit,
    int16_t xMin,
    int16_t xMax,
    int16_t yMin,
    int16_t yMax,
    int16_t buttonHeight,
    float valueTop,
    float valueBottom,
    float initVal,
    _Bool active);

/* Associating `valueTop' with the topmost position of the
 * slider, and `valueBottom' with the bottom-most position,
 * return the value between corresponding to the button position,
 * in a linear fashion. */
float ValuedVertSlideBar_getValue(ValuedVertSlideBar_t *this);

/* Set the `slideBar.buttonPos' to a value with which calling
 * `ValuedVertSlideBar_getValue' on the resulting `*this' would
 * return `value'. */
void ValuedVertSlideBar_setValue(
    ValuedVertSlideBar_t *this,
    float value);

/* Set the slide bar according to the touch coordinate
 * `y'. Guards from falling out of available range. */
void ValuedVertSlideBar_setButtonPos_touch(
    ValuedVertSlideBar_t *this,
    int16_t y);

void ValuedVertSlideBar_display(ValuedVertSlideBar_t *this);


#endif /* APPLICATION_USER_SLIDE_BUTTON_H_ */
