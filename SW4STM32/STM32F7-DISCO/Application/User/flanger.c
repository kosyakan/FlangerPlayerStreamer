#include <flanger.h>
#include <circ_buffer.h>
#include <math.h>

#define M_PIf ((float)M_PI)

FlangingFilterState_t FlangingFilterState_make(
    float delay,
    float width,
    float speed,
    float depth)
{
    FlangingFilterState_t result;
    result.delay = delay;
    result.width = width;
    result.speed = speed;
    result.depth = depth;
    result.time  = 0;
    return result;
}

static inline int ms2samples(
    float msValue,
    uint32_t samplingFreq)
{
    return (int)((msValue / 1000.0f) * samplingFreq);
}

/*
 * samplingFreq : [samples / s]
 * delay        : [ms]
 * result       : [samples]
 */
int FlangingFilterState_delaySamples(
    FlangingFilterState_t *this,
    uint32_t samplingFreq)
{
    return ms2samples(this->delay, samplingFreq);
}

/*
 * samplingFreq : [samples / s]
 * width        : [ms]
 * result       : [samples]
 */
int FlangingFilterState_widthSamples(
    FlangingFilterState_t *this,
    uint32_t samplingFreq)
{
    return ms2samples(this->width, samplingFreq);
}

/*
 * samplingFreq : [samples / s]
 * speed        : [Hz] = [1 / s]
 * result       : [1] = [1 / samples]
 */
inline float FlangingFilterState_speedUnitless(
    FlangingFilterState_t *this,
    uint32_t samplingFreq)
{
    /*
     * 2 * pi * speedHerz * tSeconds
     *  = 2 * pi * speedHerz * (tSamples / samplingFreq)
     *  = (2 * pi * speedHerz / samplingFreq) * tSamples
     *  = result * tSamples
     */
    return 2 * M_PIf * this->speed / samplingFreq;
}

inline float FlangingFilterState_depthUnitless(
    FlangingFilterState_t *this)
{
    return this->depth / 100.0f;
}

inline void FlangingFilterState_forward(
    FlangingFilterState_t *this,
    uint32_t samples)
{
    /* Filtering time @ 44.1 kHz sampling before overflow: ~27
     * hours. Increments can be taken modulo `D(t)' period
     * eventually. */
    this->time += samples;
}

#define SAMPLE_TO_FLOAT(sample)  ((float)((int16_t)sample))
#define FLOAT_TO_SAMPLE(val)     (uint16_t)((int16_t)val)

FRESULT FlangingFilterState_filterAudio(
    FlangingFilterState_t *filter,
    CircChunkBuffer_t *inBuf,
    FRESULT (*inBufForwardChunk)(CircChunkBuffer_t *this),
    CircChunkBuffer_t *outBuf,
    uint32_t samplingFreq,
    uint16_t channelsNum)
{
    /* Get filter parameters in more suitable units */
    /* `D0' */
    int baseDelaySamples =
        FlangingFilterState_delaySamples(filter, samplingFreq);
    /* `A' */
    int widthSamples =
        FlangingFilterState_widthSamples(filter, samplingFreq);
    /* `f' */
    float speedUnitless =
        FlangingFilterState_speedUnitless(filter, samplingFreq);
    /* `w' */
    float depthUnitless =
        FlangingFilterState_depthUnitless(filter);
    /* Move filtered samples from file buffer to audio buffer, up
     * to now */
    FRESULT fResult = inBufForwardChunk(inBuf);
    /* No matter if there was an IO error or not, the filter can
     * proceed for this chunk, using zeros eventually. There will
     * be just no more chunks as the player will move to
     * `AUDIO_STATE_STOP' state and stop the streaming. */
    CircChunkBuffer_forwardChunk(outBuf);

    /*
     * In the typical case of two-channel audio we have the
     * following data layout in the wav file:
     *
     * ,----
     * | +------------+------------+------------+------------+---
     * | |      L     |      P     |      L     |      P     | ...
     * | +------------+------------+------------+------------+---
     * | +--sample_t--+
     * | +---channels_sample_t-----+
     * `----
     *
     * `outBuf->chunkSize' is given in `sample_t' units,
     * `channelsSamples' is given in the hypothetical
     * `channels_sample_t' type units.
     */
    uint32_t channelsSamples = outBuf->chunkSize / channelsNum;
    FlangingFilterState_forward(filter, channelsSamples);

    int k, c;
    /* It's important to start filling the oldest samples first
     * in case the filter was started late and/or takes a lot of
     * time relatively to the audio byterate, and another half of
     * the audio buffer is read in full before this algorithm
     * ends. */
    for (k = (int)channelsSamples - 1; k >= 0; --k) {
        int t = channelsNum * k;
        /* `D(t) = D0 + A sin(f t)' */
        int delaySamples = baseDelaySamples
            + (widthSamples
               * sin(speedUnitless * (filter->time - k)));
        for (c = 0; c < channelsNum; ++c) {
            int i = -(t + c);
            /* `f(t)' */
            sample_t origSample =
                CircChunkBuffer_get(inBuf, i);
            /* `f(t - D(t))' */
            sample_t delayedSample =
                CircChunkBuffer_get(
                    inBuf, i - delaySamples * channelsNum);
            float combined =
                ((1.0f - depthUnitless) * SAMPLE_TO_FLOAT(origSample)
                 + depthUnitless * SAMPLE_TO_FLOAT(delayedSample));
            CircChunkBuffer_set(
                outBuf, i,
                FLOAT_TO_SAMPLE(combined));
        }
    }
    return fResult;
}
