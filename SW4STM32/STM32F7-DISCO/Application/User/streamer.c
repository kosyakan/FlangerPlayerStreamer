#include <streamer.h>
#include <flanger_slidebars.h>
#include <lcd_log_conf.h>

/* Relates to both "in" and "out" audio buffer - their block size
 * must be the same */
#define AUDIO_BLOCK_SIZE  ((uint32_t)512) /* [sample] */

/* The "blocks" of the audio-in buffer are not related here to
 * its two halves mentioned by the
 * `BSP_AUDIO_IN_HalfTransfer_CallBack' and
 * `BSP_AUDIO_IN_TransferComplete_CallBack' functions called by
 * the audio module. They are just amounts of data read by the
 * filter in one portion. */
#define AUDIO_IN_BLOCKS_NUM    5
/* The blocks of the audio-out buffer ARE related to its two
 * halves though, and to the ping-pong buffer emptying and
 * filling mechanism driven by
 * `BSP_AUDIO_OUT_HalfTransfer_CallBack' and
 * `BSP_AUDIO_OU_TransferComplete_CallBack' functions. This
 * parameter is not adjustable. */
#define AUDIO_OUT_BLOCKS_NUM   2

#define AUDIO_IN_BUF_SIZE \
    (AUDIO_BLOCK_SIZE * AUDIO_IN_BLOCKS_NUM) /* [sample] */
#define AUDIO_OUT_BUF_SIZE \
    (AUDIO_BLOCK_SIZE * AUDIO_OUT_BLOCKS_NUM) /* [sample] */

#define AUDIO_IN_BUF_SIZE_BYTES (AUDIO_IN_BUF_SIZE * sizeof(uint16_t))
#define AUDIO_OUT_BUF_SIZE_BYTES (AUDIO_OUT_BUF_SIZE * sizeof(uint16_t))

#define AUDIO_IN_BUF    AUDIO_REC_START_ADDR     /* In SDRAM */
#define AUDIO_OUT_BUF \
        (AUDIO_REC_START_ADDR + AUDIO_IN_BUF_SIZE_BYTES) /* In SDRAM */

/* Callbacks updating this variable are defined in "waverecorder.c" */
__IO uint32_t audio_rec_buffer_state;

static ValuedVertSlideBar_t flangerSlideBars[FLANGER_SLIDEBARS_NUM];
static CircChunkBuffer_t audioIn;
static CircChunkBuffer_t audioOut;
static FlangingFilterState_t filter;

void AUDIO_STREAMER_Init() {
    FlangerSlidebars_init(flangerSlideBars);
}

void streamerProcessButtons() {
    static uint8_t prev_touchDetected = 0;
    TS_StateTypeDef TS_State = { 0 };
    BSP_TS_GetState(&TS_State);
    if (prev_touchDetected != TS_State.touchDetected) {
        if (TS_State.touchDetected == 1) {
            Point_t touch = { TS_State.touchX[0],
                              TS_State.touchY[0] };
            FlangerSlidebars_activateTouchedSlidebar(
                flangerSlideBars,
                touch);
        } else {
            FlangerSlidebars_deactivateAllSlidebars(
                flangerSlideBars);
        }
        prev_touchDetected = TS_State.touchDetected;
    }
    else if (TS_State.touchDetected == 1) {
        FlangerSlidebars_moveActiveSlidebars(
            flangerSlideBars,
            &filter,
            TS_State.touchY[0]);
    }
}

static void displayStreamer() {
    /* Clean the whole screen except the title */
    for (int i = 3; i < 25; ++i) {
        BSP_LCD_ClearStringLine(i);
    }
    FlangerSlidebars_display(flangerSlideBars);

    BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
    BSP_LCD_SetFont(&LCD_LOG_TEXT_FONT);
    //BSP_LCD_ClearStringLine(15);
    BSP_LCD_DisplayStringAtLine(
        20, (uint8_t*) "Use on-board mems microphones and earphones to filter live audio");
    BSP_LCD_DisplayStringAtLine(
        21, (uint8_t*) "Reset to return");
}

void AUDIO_STREAMER_Process() {

    displayStreamer();
    
    FlangerSlidebars_updateFilterStateAll(flangerSlideBars, &filter);

    /* In case of the `audioIn' buffer the concrete partitioning
     * into chunks, their number and size, don't really matter,
     * as it will be filled by the audio module in parallel and
     * callbacks `BSP_AUDIO_IN_HalfTransfer_CallBack' and
     * `BSP_AUDIO_IN_TransferComplete_CallBack' will be
     * ignored. The audio data movement and filtering will be
     * driven by `audioOut' buffer emptying callbacks. What
     * matters in the case of `audioIn' is the total size in
     * samples - `AUDIO_IN_BUF_SIZE' - which must be big enough
     * to allow the flanging filter to reach samples up to around
     * 30 [ms] in the past. */

    audioIn = CircChunkBuffer_make(
        (uint16_t*) AUDIO_IN_BUF,
        AUDIO_IN_BUF_SIZE,
        AUDIO_BLOCK_SIZE);

    audioOut = CircChunkBuffer_make(
        (uint16_t*) AUDIO_OUT_BUF,
        AUDIO_OUT_BUF_SIZE,
        AUDIO_BLOCK_SIZE);

    /* Initialize SDRAM buffers */
    memset((uint16_t*) AUDIO_IN_BUF, 0,
           AUDIO_IN_BUF_SIZE_BYTES);
    memset((uint16_t*) AUDIO_OUT_BUF, 0,
           AUDIO_OUT_BUF_SIZE_BYTES);

    /* Initialize Audio Recorder */
    if (BSP_AUDIO_IN_OUT_Init(
            INPUT_DEVICE_DIGITAL_MICROPHONE_2,
            OUTPUT_DEVICE_HEADPHONE,
            DEFAULT_AUDIO_IN_FREQ,
            DEFAULT_AUDIO_IN_BIT_RESOLUTION,
            DEFAULT_AUDIO_IN_CHANNEL_NBR) != AUDIO_OK) {
        USBH_UsrLog("  AUDIO RECORD INIT FAIL");        
    }

    audio_rec_buffer_state = STREAMER_BUFFER_OFFSET_NONE;
    
    /* Start Recording */
    BSP_AUDIO_IN_Record((uint16_t*) AUDIO_IN_BUF,
                        AUDIO_IN_BUF_SIZE);
    
    HAL_Delay(10);
    
    /* Start Playback */
    BSP_AUDIO_OUT_SetAudioFrameSlot(CODEC_AUDIOFRAME_SLOT_02);
    BSP_AUDIO_OUT_Play((uint16_t*) AUDIO_OUT_BUF,
                       AUDIO_OUT_BUF_SIZE_BYTES);

    while (1) {
        switch (audio_rec_buffer_state) {
        case STREAMER_BUFFER_OFFSET_NONE: {
            streamerProcessButtons();
            break;
        }
        case STREAMER_BUFFER_OFFSET_HALF:
        case STREAMER_BUFFER_OFFSET_FULL: {
            FlangingFilterState_filterAudio(
                &filter, &audioIn,
                CircChunkBuffer_forwardChunk, &audioOut,
                DEFAULT_AUDIO_IN_FREQ,
                DEFAULT_AUDIO_IN_CHANNEL_NBR);
            audio_rec_buffer_state = STREAMER_BUFFER_OFFSET_NONE;
            break;
        }
        }
    }
}
