BUILD_CONF = SW4STM32/STM32F7-DISCO/Debug/

all:
	$(MAKE) all -C $(BUILD_CONF)
	@ echo
	@ echo "Binaries in: $(BUILD_CONF)"
	@ echo

.PHONY: clean
clean:
	$(MAKE) clean -C $(BUILD_CONF)
