/**
 ******************************************************************************
 * @file    Audio/Audio_playback_and_record/Src/waveplayer.c
 * @author  MCD Application Team
 * @brief   This file provides the Audio Out (playback) interface API
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2016 STMicroelectronics International N.V.
 * All rights reserved.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "waveplayer.h"

#include <streamer.h>
#include <buttons.h>

/* Private define ------------------------------------------------------------*/
#define TOUCH_NEXT_XMIN         325
#define TOUCH_NEXT_XMAX         365
#define TOUCH_NEXT_YMIN         212
#define TOUCH_NEXT_YMAX         252

#define TOUCH_PREVIOUS_XMIN     250
#define TOUCH_PREVIOUS_XMAX     290
#define TOUCH_PREVIOUS_YMIN     212
#define TOUCH_PREVIOUS_YMAX     252

#define TOUCH_STOP_XMIN         170
#define TOUCH_STOP_XMAX         210
#define TOUCH_STOP_YMIN         212
#define TOUCH_STOP_YMAX         252

#define TOUCH_PAUSE_XMIN        100
#define TOUCH_PAUSE_XMAX        124
#define TOUCH_PAUSE_YMIN        212
#define TOUCH_PAUSE_YMAX        252

#define TOUCH_VOL_MINUS_XMIN    20
#define TOUCH_VOL_MINUS_XMAX    70
#define TOUCH_VOL_MINUS_YMIN    212
#define TOUCH_VOL_MINUS_YMAX    252

#define TOUCH_VOL_PLUS_XMIN     402
#define TOUCH_VOL_PLUS_XMAX     452
#define TOUCH_VOL_PLUS_YMIN     212
#define TOUCH_VOL_PLUS_YMAX     252


/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static AUDIO_OUT_BufferTypeDef BufferCtl;
static int16_t FilePos = 0;
static __IO uint32_t uwVolume = 70;
static Point NextPoints[] = { { TOUCH_NEXT_XMIN, TOUCH_NEXT_YMIN }, {
TOUCH_NEXT_XMAX, (TOUCH_NEXT_YMIN + TOUCH_NEXT_YMAX) / 2 }, {
TOUCH_NEXT_XMIN, TOUCH_NEXT_YMAX } };
static Point PreviousPoints[] = { { TOUCH_PREVIOUS_XMIN, (TOUCH_PREVIOUS_YMIN
        + TOUCH_PREVIOUS_YMAX) / 2 },
        { TOUCH_PREVIOUS_XMAX, TOUCH_PREVIOUS_YMIN }, { TOUCH_PREVIOUS_XMAX,
        TOUCH_PREVIOUS_YMAX } };

WAVE_FormatTypeDef WaveFormat;
FIL WavFile;
extern FILELIST_FileTypeDef FileList;

/* Private function prototypes -----------------------------------------------*/
static AUDIO_ErrorTypeDef GetFileInfo(uint16_t file_idx,
        WAVE_FormatTypeDef *info);
// static uint8_t PlayerInit(uint32_t AudioFreq);
static void AUDIO_PlaybackDisplayButtons(void);

static void processButtons();

static void waveplayerButtonsProcessState(Point_t touch);

/* Private functions ---------------------------------------------------------*/


/*****************************************************************************/
/*                     Author: Marcin Wcislo (beginning)                     */
/* vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv */

/* Buttons *******************************************************************/

#include <buttons.h>

/*
 * Position of the
 *
 * ,----
 * | [00:00]
 * | [PLAY ]
 * `----
 *
 * info
 */

#define ELAPSED_TIME_X 0
#define ELAPSED_TIME_Y LINE(11)

#define PLAY_PAUSE_STATUS_X ELAPSED_TIME_X
#define PLAY_PAUSE_STATUS_Y (ELAPSED_TIME_Y + LINE(1))

static TouchableRect_t volMinus;
static TouchableRect_t volPlus;

static Rect_t stopButton;
static Rect_t playPauseButton;
static Rect_t prevTrackButton;
static Rect_t nextTrackButton;

#include <flanger_slidebars.h>

static ValuedVertSlideBar_t flangerSlideBars[FLANGER_SLIDEBARS_NUM];

#define TOUCHABLE_RECTS_SIZE 6
// For the convenience of detecting touch events
static TouchableRect_t* touchableRects[] = {
    &volMinus,
    &volPlus,
    &flangerSlideBars[DELAY_SLIDE_BAR].slideBar.frame,
    &flangerSlideBars[DEPTH_SLIDE_BAR].slideBar.frame,
    &flangerSlideBars[WIDTH_SLIDE_BAR].slideBar.frame,
    &flangerSlideBars[SPEED_SLIDE_BAR].slideBar.frame,
};

/* File and audio buffers, filter ********************************************/

#include "flanger.h"
#include "circ_buffer.h"

static FlangingFilterState_t filter;

/* Audio buffer */

/* Not customizable, determined by the audio module providing two
 * callback functions:
 * `BSP_AUDIO_OUT_TransferComplete_CallBack'
 * `BSP_AUDIO_OUT_HalfTransfer_CallBack'
 * splitting the audio buffer effectively into 2 chunks.
 */
#define AUDIO_BUF_CHUNKS_NUM 2

/* The smaller the higher resolution of filter parameters
 * changing and better user experience (filter parameters remain
 * constant for the time of emptying data buffer this long.
 *
 * It cannot be too small, however, to not let the
 * `aqcuireNewAudioDataChunk' function exit before the audio module empty
 * the buffer of this many samples.
 *
 * The actual physical time of emptying the buffer depends on the
 * .wav file's paraticular sampling rate). See the included in
 * the project spreadsheet "buffer-parameters.ods" for the
 * calculation of safe values.
 *
 */
#define AUDIO_BUF_CHUNK_SIZE 4096 /* [samples] */

#define AUDIO_BUF_SIZE \
    AUDIO_BUF_CHUNK_SIZE * AUDIO_BUF_CHUNKS_NUM

static sample_t audioBufData[AUDIO_BUF_SIZE];

/* Control structure operating on `fileBufData' */
static CircChunkBuffer_t audioBuf;


/* File buffer */

/* File buffer is being filled in chunks of size
 * `FILE_BUF_CHUNK_SIZE', on demand, when needed by the
 * `audioBuf' control struct. The chunks can (and probably
 * should) be bigger than the audio chunks of size
 * `AUDIO_BUF_CHUNKSIZE', but shouldn't be too big as well, in
 * order not to lock the audio streaming. The total size of the
 * `fileBufData' directly correlates with the maximum delay in
 * the flanging filter. See the included in the project
 * spreadsheet "buffer-parameters.ods" for the calculation of
 * safe values.
 */
#define FILE_BUF_CHUNKS_NUM 3
#define FILE_BUF_CHUNK_SIZE 6096 /* [samples] */

#define FILE_BUF_SIZE \
    FILE_BUF_CHUNKS_NUM * FILE_BUF_CHUNK_SIZE

static sample_t fileBufData[FILE_BUF_SIZE];

/* Control structure operating on `fileBufData' */
static FileReadingBuffer_t fileBuf;

/* Implementations ***********************************************************/

void display_volMinus(TouchableRect_t *this) {
    TouchableRect_display(this);
    BSP_LCD_DisplayStringAt(24, LINE(14), (uint8_t*) "VOl-", LEFT_MODE);
}

void display_volPlus(TouchableRect_t *this) {
    TouchableRect_display(this);
    BSP_LCD_DisplayStringAt(24, LINE(14), (uint8_t*) "VOl+", RIGHT_MODE);
}

void displayElapsedTime(uint32_t elapsed_time) {
    uint8_t str[20];
    static uint32_t prev_elapsed_time = 0xFFFFFFFF;
    uint32_t currColor = BSP_LCD_GetTextColor();
    if (prev_elapsed_time != elapsed_time) {
        prev_elapsed_time = elapsed_time;
        sprintf((char*) str, "  [%02d:%02d]",
                (int) (elapsed_time / 60),
                (int) (elapsed_time % 60));
        BSP_LCD_SetTextColor(LCD_COLOR_CYAN);
        BSP_LCD_DisplayStringAt(ELAPSED_TIME_X,
                                ELAPSED_TIME_Y,
                                str,
                                LEFT_MODE);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    }
    BSP_LCD_SetTextColor(currColor);
}

void displayPlayPauseStatus(_Bool play) {
    uint32_t currColor = BSP_LCD_GetTextColor();
    BSP_LCD_SetTextColor(LCD_COLOR_CYAN);
    static int prev_play = -1;
    if (prev_play != (int)play) {
        if (play) {
            BSP_LCD_DisplayStringAt(PLAY_PAUSE_STATUS_X,
                                    PLAY_PAUSE_STATUS_Y,
                                    (uint8_t*) "  [PLAY ]",
                                    LEFT_MODE);
        } else {
            BSP_LCD_DisplayStringAt(PLAY_PAUSE_STATUS_X,
                                    PLAY_PAUSE_STATUS_Y,
                                    (uint8_t*) "  [PAUSE]",
                                    LEFT_MODE);
        }
    }
    BSP_LCD_SetTextColor(currColor);
}

static FRESULT aqcuireNewAudioDataChunk() {
    return FlangingFilterState_filterAudio(
        &filter,
        (CircChunkBuffer_t*)&fileBuf,
        FileReadingBuffer_forwardChunk,
        &audioBuf,
        WaveFormat.SampleRate, WaveFormat.NbrChannels);
}

#define DEFINE_BUTTON_RECT(button, NAME)        \
    button = Rect_make(                         \
        TOUCH_ ## NAME ## _XMIN,                \
        TOUCH_ ## NAME ## _XMAX,                \
        TOUCH_ ## NAME ## _YMIN,                \
        TOUCH_ ## NAME ## _YMAX)

/**
 * @brief  Initializes Audio Interface.
 * @param  None
 * @retval Audio error
 */
AUDIO_ErrorTypeDef AUDIO_PLAYER_Init(void) {

    FlangerSlidebars_init(flangerSlideBars);
    
    volMinus = TouchableRect_make(
        TOUCH_VOL_MINUS_XMIN,
        TOUCH_VOL_MINUS_XMAX,
        TOUCH_VOL_MINUS_YMIN,
        TOUCH_VOL_MINUS_YMAX,
        0, 0);

    volPlus = TouchableRect_make(
        TOUCH_VOL_PLUS_XMIN,
        TOUCH_VOL_PLUS_XMAX,
        TOUCH_VOL_PLUS_YMIN,
        TOUCH_VOL_PLUS_YMAX,
        0, 0);

    DEFINE_BUTTON_RECT(stopButton, STOP);
    DEFINE_BUTTON_RECT(playPauseButton, PAUSE);
    DEFINE_BUTTON_RECT(prevTrackButton, PREVIOUS);
    DEFINE_BUTTON_RECT(nextTrackButton, NEXT);

    if (BSP_AUDIO_OUT_Init(
            OUTPUT_DEVICE_AUTO,
            uwVolume,
            I2S_AUDIOFREQ_48K) == 0) {
        return AUDIO_ERROR_NONE;
    } else {
        return AUDIO_ERROR_IO;
    }
}

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
/*                        Author: Marcin Wcislo (end)                        */
/*****************************************************************************/

/**
 * @brief  Starts Audio streaming.
 * @param  idx: File index
 * @retval Audio error
 */
AUDIO_ErrorTypeDef AUDIO_PLAYER_Start(uint8_t idx) {
    f_close(&WavFile);
    if (AUDIO_GetWavObjectNumber() > idx) {
        GetFileInfo(idx, &WaveFormat);

        /* Initialize the Audio codec and all related peripherals
         * (I2S, I2C, IOExpander, IOs...) */
        if (BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_BOTH,
                               uwVolume,
                               WaveFormat.SampleRate) == AUDIO_OK) {
            BSP_AUDIO_OUT_SetAudioFrameSlot(CODEC_AUDIOFRAME_SLOT_02);
        }

        BufferCtl.state = BUFFER_OFFSET_NONE;

        /* Get Data from USB Flash Disk */
        f_lseek(&WavFile, 0);

/*****************************************************************************/
/*                     Author: Marcin Wcislo (beginning)                     */
/* vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv */

        /* Initialize filter's parameters with current slidebars
         * settings */
        FlangerSlidebars_updateFilterStateAll(
            flangerSlideBars,
            &filter);
                
        fileBuf = FileReadingBuffer_make(
            fileBufData,
            FILE_BUF_CHUNK_SIZE,
            FILE_BUF_CHUNKS_NUM,
            AUDIO_BUF_CHUNK_SIZE,
            &WavFile,
            WaveFormat.FileSize,
            WaveFormat.ByteRate,
            /* Setting width bigger than the current base delay
             * implies using values from the future, so convert
             * the maximum difference being `WIDTH_VALUE_MAX' in
             * [ms], convert it to samples, and pass as the
             * `futureSamplesMargin' arg. */
            (uint32_t)((WIDTH_VALUE_MAX / 1000.f)
                       * WaveFormat.SampleRate
                       * WaveFormat.NbrChannels));

        audioBuf = CircChunkBuffer_make(
            audioBufData,
            AUDIO_BUF_SIZE,
            AUDIO_BUF_CHUNK_SIZE);
        /* Fill the whole `audioBuf' with initial filtered data
         * from the wav file. The process will be continued later
         * in `BSP_AUDIO_OUT_HalfTransfer_CallBack' and
         * `BSP_AUDIO_OUT_TransferComplete_CallBack' */
        FRESULT fResult = FR_OK;
        for (int i = 0; i < AUDIO_BUF_CHUNKS_NUM; ++i) {
            fResult = aqcuireNewAudioDataChunk();
        }
        if (fResult == FR_OK) {
            AudioState = AUDIO_STATE_PLAY;
            AUDIO_PlaybackDisplayButtons();
            displayPlayPauseStatus(1);
            BSP_AUDIO_OUT_Play(
                audioBufData,
                AUDIO_BUF_SIZE * sizeof(sample_t));
            return AUDIO_ERROR_NONE;
        } else {
            return AUDIO_ERROR_IO;
        }

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
/*                        Author: Marcin Wcislo (end)                        */
/*****************************************************************************/

    }
    return AUDIO_ERROR_IO;
}

/**
 * @brief  Manages Audio process.
 * @param  None
 * @retval Audio error
 */
AUDIO_ErrorTypeDef AUDIO_PLAYER_Process(void) {
    AUDIO_ErrorTypeDef audio_error = AUDIO_ERROR_NONE;
    uint8_t str[16];

    switch (AudioState) {
    case AUDIO_STATE_PLAY:

/*****************************************************************************/
/*                     Author: Marcin Wcislo (beginning)                     */
/* vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv */

        if (FileReadingBuffer_isFileFinished(&fileBuf)) {
            BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);
            AudioState = AUDIO_STATE_NEXT;
        }

        if (BufferCtl.state == BUFFER_OFFSET_HALF
            || BufferCtl.state == BUFFER_OFFSET_FULL) {
            if (aqcuireNewAudioDataChunk() != FR_OK) {
                BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);
                return AUDIO_ERROR_IO;
            }
            BufferCtl.state = BUFFER_OFFSET_NONE;
        }

        displayElapsedTime(FileReadingBuffer_elapsedTime(&fileBuf));

        processButtons();
        break;

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
/*                        Author: Marcin Wcislo (end)                        */
/*****************************************************************************/

    case AUDIO_STATE_STOP:
        BSP_LCD_SetTextColor(LCD_COLOR_RED);
        BSP_LCD_FillRect(TOUCH_STOP_XMIN, TOUCH_STOP_YMIN, /* Stop rectangle */
                         TOUCH_STOP_XMAX - TOUCH_STOP_XMIN,
                         TOUCH_STOP_YMAX - TOUCH_STOP_YMIN);
        BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);
        AudioState = AUDIO_STATE_IDLE;
        audio_error = AUDIO_ERROR_IO;
        break;

    case AUDIO_STATE_NEXT:
        if (++FilePos >= AUDIO_GetWavObjectNumber()) {
            FilePos = 0;
        }
        BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);
        AUDIO_PLAYER_Start(FilePos);
        if (uwVolume == 0) {
            BSP_AUDIO_OUT_SetVolume(uwVolume);
        }
        break;

    case AUDIO_STATE_PREVIOUS:
        if (--FilePos < 0) {
            FilePos = AUDIO_GetWavObjectNumber() - 1;
        }
        BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);
        AUDIO_PLAYER_Start(FilePos);
        if (uwVolume == 0) {
            BSP_AUDIO_OUT_SetVolume(uwVolume);
        }
        break;

    case AUDIO_STATE_PAUSE:
        displayPlayPauseStatus(0);
        BSP_LCD_SetTextColor(LCD_COLOR_RED); /* Display red pause rectangles */
        BSP_LCD_FillRect(TOUCH_PAUSE_XMIN, TOUCH_PAUSE_YMIN, 15,
        TOUCH_PAUSE_YMAX - TOUCH_PAUSE_YMIN);
        BSP_LCD_FillRect(TOUCH_PAUSE_XMIN + 20, TOUCH_PAUSE_YMIN, 15,
        TOUCH_PAUSE_YMAX - TOUCH_PAUSE_YMIN);
        BSP_AUDIO_OUT_Pause();
        AudioState = AUDIO_STATE_WAIT;
        break;

    case AUDIO_STATE_RESUME:
        displayPlayPauseStatus(1);
        /* Display blue cyan pause rectangles */
        BSP_LCD_FillRect(TOUCH_PAUSE_XMIN, TOUCH_PAUSE_YMIN, 15,
        TOUCH_PAUSE_YMAX - TOUCH_PAUSE_YMIN);
        BSP_LCD_FillRect(TOUCH_PAUSE_XMIN + 20, TOUCH_PAUSE_YMIN, 15,
        TOUCH_PAUSE_YMAX - TOUCH_PAUSE_YMIN);
        BSP_AUDIO_OUT_Resume();
        if (uwVolume == 0) {
            BSP_AUDIO_OUT_SetVolume(uwVolume);
        }
        AudioState = AUDIO_STATE_PLAY;
        break;

    case AUDIO_STATE_VOLUME_UP:
        if (uwVolume <= 90) {
            uwVolume += 10;
        }
        BSP_AUDIO_OUT_SetVolume(uwVolume);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        sprintf((char*) str, "Volume : %lu ", uwVolume);
        BSP_LCD_DisplayStringAtLine(9, str);
        AudioState = AUDIO_STATE_PLAY;
        break;

    case AUDIO_STATE_VOLUME_DOWN:
        if (uwVolume >= 10) {
            uwVolume -= 10;
        }
        BSP_AUDIO_OUT_SetVolume(uwVolume);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        sprintf((char*) str, "Volume : %lu ", uwVolume);
        BSP_LCD_DisplayStringAtLine(9, str);
        AudioState = AUDIO_STATE_PLAY;
        break;

    case AUDIO_STATE_WAIT:
    case AUDIO_STATE_IDLE:
    case AUDIO_STATE_INIT:
    default:
        /* Update audio state machine according to touch acquisition */
        processButtons();
        break;
    }
    return audio_error;
}


extern AUDIO_DEMO_StateMachine AudioDemo;
extern __IO uint32_t audio_rec_buffer_state;
/**
 * @brief  Calculates the remaining file size and new position of the pointer.
 * @param  None
 * @retval None
 */
void BSP_AUDIO_OUT_TransferComplete_CallBack(void) {
    if (AudioDemo.state == AUDIO_DEMO_PLAYBACK) {
        if (AudioState == AUDIO_STATE_PLAY) {
            BufferCtl.state = BUFFER_OFFSET_FULL;
        }
    } else if (AudioDemo.state == AUDIO_DEMO_STREAM) {
        audio_rec_buffer_state = STREAMER_BUFFER_OFFSET_FULL;
    }
}

/**
 * @brief  Manages the DMA Half Transfer complete interrupt.
 * @param  None
 * @retval None
 */
void BSP_AUDIO_OUT_HalfTransfer_CallBack(void) {
    if (AudioDemo.state == AUDIO_DEMO_PLAYBACK) {
        if (AudioState == AUDIO_STATE_PLAY) {
            BufferCtl.state = BUFFER_OFFSET_HALF;
        }
    } else if (AudioDemo.state == AUDIO_DEMO_STREAM) {
        audio_rec_buffer_state = STREAMER_BUFFER_OFFSET_HALF;
    }
}


/*******************************************************************************
 Static Functions
 *******************************************************************************/

/**
 * @brief  Gets the file info.
 * @param  file_idx: File index
 * @param  info: Pointer to WAV file info
 * @retval Audio error
 */
static AUDIO_ErrorTypeDef GetFileInfo(uint16_t file_idx,
        WAVE_FormatTypeDef *info) {
    uint32_t bytesread;
    uint32_t duration;
    uint8_t str[FILEMGR_FILE_NAME_SIZE + 20];

    if (f_open(&WavFile, (char*) FileList.file[file_idx].name,
    FA_OPEN_EXISTING | FA_READ) == FR_OK) {
        /* Fill the buffer to Send */
        if (f_read(&WavFile, info, sizeof(WaveFormat), (void*) &bytesread)
                == FR_OK) {
            BSP_LCD_SetTextColor(LCD_COLOR_WHITE);

            sprintf((char*) str, "Playing file (%d/%d):", file_idx + 1,
                    FileList.ptr);
            BSP_LCD_ClearStringLine(4);
            BSP_LCD_DisplayStringAtLine(4, str);

            sprintf((char*) str, "%s", (char*) FileList.file[file_idx].name);
            BSP_LCD_ClearStringLine(5);
            BSP_LCD_DisplayStringAtLine(5, str);

            BSP_LCD_SetTextColor(LCD_COLOR_CYAN);
            sprintf((char*) str, "Sample rate : %d Hz",
                    (int) (info->SampleRate));
            BSP_LCD_ClearStringLine(6);
            BSP_LCD_DisplayStringAtLine(6, str);

            sprintf((char*) str, "Channels number : %d", info->NbrChannels);
            BSP_LCD_ClearStringLine(7);
            BSP_LCD_DisplayStringAtLine(7, str);

            duration = info->FileSize / info->ByteRate;
            sprintf((char*) str, "File Size : %d KB [%02d:%02d]",
                    (int) (info->FileSize / 1024), (int) (duration / 60),
                    (int) (duration % 60));
            BSP_LCD_ClearStringLine(8);
            BSP_LCD_DisplayStringAtLine(8, str);

            // displayPlayInfo(0, AudioState == AUDIO_STATE_PLAY);
            displayElapsedTime(0);
            // BSP_LCD_DisplayStringAt(263, LINE(8), (uint8_t*) "[00:00]",
            //         LEFT_MODE);

            BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
            sprintf((char*) str, "Volume : %lu", uwVolume);
            BSP_LCD_ClearStringLine(9);
            BSP_LCD_DisplayStringAtLine(9, str);
            return AUDIO_ERROR_NONE;
        }
        f_close(&WavFile);
    }
    return AUDIO_ERROR_IO;
}

/**
 * @brief  Display interface touch screen buttons
 * @param  None
 * @retval None
 */
static void AUDIO_PlaybackDisplayButtons(void) {
    BSP_LCD_SetFont(&LCD_LOG_HEADER_FONT);
    BSP_LCD_ClearStringLine(13); /* Clear dedicated zone */
    BSP_LCD_ClearStringLine(14);
    BSP_LCD_ClearStringLine(15);

    BSP_LCD_SetTextColor(LCD_COLOR_CYAN);
    /* Previous track icon */
    BSP_LCD_FillPolygon(PreviousPoints, 3);
    BSP_LCD_FillRect(TOUCH_PREVIOUS_XMIN, TOUCH_PREVIOUS_YMIN, 10,
                     TOUCH_PREVIOUS_YMAX - TOUCH_PREVIOUS_YMIN);
    /* Next track icon */
    BSP_LCD_FillPolygon(NextPoints, 3);
    BSP_LCD_FillRect(TOUCH_NEXT_XMAX - 9, TOUCH_NEXT_YMIN, 10,
                     TOUCH_NEXT_YMAX - TOUCH_NEXT_YMIN);
    /* Pause rectangles */
    BSP_LCD_FillRect(TOUCH_PAUSE_XMIN, TOUCH_PAUSE_YMIN, 15,
                     TOUCH_PAUSE_YMAX - TOUCH_PAUSE_YMIN);
    BSP_LCD_FillRect(TOUCH_PAUSE_XMIN + 20, TOUCH_PAUSE_YMIN, 15,
                     TOUCH_PAUSE_YMAX - TOUCH_PAUSE_YMIN);
    /* Stop rectangle */
    BSP_LCD_FillRect(TOUCH_STOP_XMIN, TOUCH_STOP_YMIN,
                     TOUCH_STOP_XMAX - TOUCH_STOP_XMIN,
                     TOUCH_STOP_YMAX - TOUCH_STOP_YMIN);

    display_volMinus(&volMinus);
    display_volPlus(&volPlus);
    FlangerSlidebars_display(flangerSlideBars);
    
    BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
    BSP_LCD_SetFont(&LCD_LOG_TEXT_FONT);
    //BSP_LCD_ClearStringLine(15);
    BSP_LCD_DisplayStringAtLine(15, (uint8_t*) "Use stop button to exit");
    BSP_LCD_SetTextColor(LCD_COLOR_CYAN);
}


/*****************************************************************************/
/*                     Author: Marcin Wcislo (beginning)                     */
/* vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv */

static void waveplayerButtonsProcessState(Point_t touch) {
    if (Rect_isPointInside(&playPauseButton, touch)) {
        if (AudioState == AUDIO_STATE_PLAY) {
            AudioState = AUDIO_STATE_PAUSE;
        } else {
            AudioState = AUDIO_STATE_RESUME;
        }
    } else if (Rect_isPointInside(&nextTrackButton, touch)) {
        AudioState = AUDIO_STATE_NEXT;
    } else if (Rect_isPointInside(&prevTrackButton, touch)) {
        AudioState = AUDIO_STATE_PREVIOUS;
    } else if (Rect_isPointInside(&stopButton, touch)) {
        AudioState = AUDIO_STATE_STOP;
    } else if (Rect_isPointInside(&volMinus.rect, touch)) {
        AudioState = AUDIO_STATE_VOLUME_DOWN;
    } else if (Rect_isPointInside(&volPlus.rect, touch)) {
        AudioState = AUDIO_STATE_VOLUME_UP;
    }
}

static void activateTouchedButtons(Point_t touch) {
    for (int i = 0; i < TOUCHABLE_RECTS_SIZE; ++i) {
        if (Rect_isPointInside(
                &touchableRects[i]->rect,
                touch)) {
            TouchableRect_setActive(touchableRects[i], 1);
        }
    }
}

static void deactivateAllButtons() {
    for (int i = 0; i < TOUCHABLE_RECTS_SIZE; ++i) {
        TouchableRect_setActive(touchableRects[i], 0);
    }
}

void processButtons() {
    static uint8_t prev_touchDetected = 0;
    TS_StateTypeDef TS_State = { 0 };

    BSP_TS_GetState(&TS_State);
    if (prev_touchDetected != TS_State.touchDetected) {
        if (TS_State.touchDetected == 1) {
            Point_t touch = { TS_State.touchX[0],
                              TS_State.touchY[0] };
            waveplayerButtonsProcessState(touch);
            activateTouchedButtons(touch);
        } else {
            deactivateAllButtons();
        }
        prev_touchDetected = TS_State.touchDetected;
    }
    else if (TS_State.touchDetected == 1) {
        // moveActiveSlideBars(&TS_State);
        FlangerSlidebars_moveActiveSlidebars(
            flangerSlideBars,
            &filter,
            TS_State.touchY[0]);
    }
}

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
/*                        Author: Marcin Wcislo (end)                        */
/*****************************************************************************/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
